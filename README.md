# flux-sample

This repository has a sample .net app.

To run the app install skaffold and use 
```
skaffold run
```
```
skaffold delete
```
```
skaffold build
```

NOTE: You may need to change the repository in skaffold.yaml to your repository.